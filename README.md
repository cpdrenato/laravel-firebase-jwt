# Laravel Firebase JWT

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

> **Note:** This repository contains the core code of the Laravel framework. If you want to build an application using Laravel, visit the main [Laravel repository](https://github.com/laravel/laravel).

## Resumo

- Linguagem global JSON ou XML;
- Acessa a rota enviado o JSON;

### Comandos Utilizados:
```
- composer require laravel/ui "^1.0" --dev

- php artisan ui:auth
- php artisan migrate
- php artisan db:seed
- composer dumpautoload
- php artisan serve
```
## you can make it with: echo "base64:$(openssl rand -base64 32)"
JWT_SECRET=

## Limpar Cache

```
php artisan config:clear
php artisan cache:clear
php artisan view:clear
php artisan route:clear
composer dump-autoload
php artisan optimize:clear
```

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Build Status](https://travis-ci.org/ricventu/laravel-firebase-jwt.svg?branch=master)](https://travis-ci.org/ricventu/laravel-firebase-jwt)

# laravel-firebase-jwt
[Laravel](https://github.com/laravel/laravel) with [Firebase/php-jwt](https://github.com/firebase/php-jwt) API authentication

- https://packagist.org/packages/firebase/php-jwt#v5.5.1
- https://packagist.org/packages/laravel/framework
- https://blog.renatolucena.net

## Lumen
- https://paulorb.net/guia-rapido-de-utilizacao-do-microframework-laravel-lumen/

[![Renato Lucena](//www.gravatar.com/avatar/9c822eac3f4b67e56e22f42dcc291182?s=80&amp;d=mm&amp;r=x)](https://blog.renatolucena.net)